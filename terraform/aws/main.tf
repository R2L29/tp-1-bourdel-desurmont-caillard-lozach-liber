# PROVIDER
provider "aws" {
  region = var.region
}

resource "aws_key_pair" "admin_tp" {
  key_name   = "admin_tp"
  public_key = file(var.aws_public_key_ssh_path)
}

resource "aws_default_vpc" "default_vpc_tp" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = "ami-046a9f26a7f14326b"
  instance_type = "t2.micro"

  tags = {
    Name = "tp1_test"
  }
}